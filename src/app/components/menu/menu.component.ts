import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {


  links: any[] = [
    { url: '/home', text: 'Acceuil'},
    { url: '/about', text: 'A Propos'},
    { url: '/bindings', text: 'Bindings'},
    { url: '/ex1', text: 'Chronomètre'},
    { url: '/directive', text: 'Stuctural Directive'},
    { url: '/b2w', text: 'Binding 2 ways'},
    { url: '/ex2', text: 'Panier'},
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
