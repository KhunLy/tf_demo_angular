import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './pages/about/about.component';
import { Binding2WaysComponent } from './pages/binding2-ways/binding2-ways.component';
import { BindingsComponent } from './pages/bindings/bindings.component';
import { Exercice1Component } from './pages/exercice1/exercice1.component';
import { Exercice2Component } from './pages/exercice2/exercice2.component';
import { HomeComponent } from './pages/home/home.component';
import { StructDirectiveComponent } from './pages/struct-directive/struct-directive.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  // { path: 'chemin', component: ComposantQuiSeraChargé }
  // dans l'url si on rajoute /home le composant Home sera chargé là où se trouve le router outlet
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'bindings', component: BindingsComponent },
  { path: 'ex1', component: Exercice1Component },
  { path: 'directive', component: StructDirectiveComponent },
  { path: 'b2w', component: Binding2WaysComponent },
  { path: 'ex2', component: Exercice2Component },
  { path: '**', redirectTo: '/home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

// ajouter dans le dossier pages un composant about
// ajouter le composant dans le routing