import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './struct-directive.component.html',
  styleUrls: ['./struct-directive.component.scss']
})
export class StructDirectiveComponent implements OnInit {

  open: boolean = true;

  personnes: string[] = [
    'mike', 'khun', 'thierry', 'aurelien', 'quentin'
  ];

  constructor() { }

  ngOnInit(): void {
  }

  afficherCacher() {
    this.open = !this.open;
  }

}
