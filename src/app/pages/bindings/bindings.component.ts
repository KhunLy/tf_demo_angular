import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './bindings.component.html',
  styleUrls: ['./bindings.component.scss']
})
export class BindingsComponent implements OnInit {

  //variables
  maVariable: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  augmenter() {
    // code le la methode augmenter
    this.maVariable++;
  }

  diminuer() {
    this.maVariable--;
  }

}
