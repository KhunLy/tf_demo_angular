import { Component, OnInit } from '@angular/core';
import { nanoid } from 'nanoid';
import { MessageService } from 'primeng/api';

@Component({
  templateUrl: './exercice2.component.html',
  styleUrls: ['./exercice2.component.scss'],
})
export class Exercice2Component implements OnInit {

  nouvelArticle: string = '';

  articles: any[] = [];

  constructor(
    private messageService: MessageService // injection de dépendance
  ) { }

  ngOnInit(): void {
  }

  ajouter() {
    // nettoyage des données
    let cleanArticle = this.nouvelArticle.trim().toLowerCase();
    // si dans la liste d'article j'ai déjà un article 
    if(this.articles.find(a => a.nom === cleanArticle)) {
      //j'affiche un message 
      this.messageService.add({ severity:'error', 
        summary: 'Cet article existe déjà!!' });
      //et je stoppe le processus
      return;
    }
    this.articles.push({ id: nanoid(), nom: cleanArticle, quantite: 1 });
    this.nouvelArticle = '';
  }

  retirer(article: any) { // modifier ici string -> any
    let index = this.articles.indexOf(article);
    this.articles.splice(index, 1);
  }

  augmenter(article: any) {
    article.quantite++;
  }

  diminuer(article: any) {
    article.quantite--;
    if(article.quantite <= 0) {
      this.retirer(article);
    }
  }

}


//1. Ajouter un bouton + pour augmenter la quantite
//2. Ajouter un bouton - pour diminuer la quantite (si la quantite == 0 retirer l'article)
//3. modifier la methode ajouter pour empecher d'ajouter 2 fois le meme article
