import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './binding2-ways.component.html',
  styleUrls: ['./binding2-ways.component.scss']
})
export class Binding2WaysComponent implements OnInit {

  nombre: number = 0;
  resultat: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  calculerLeCarre() {
    // this.resultat = this.nombre * this.nombre;
    this.resultat = this.nombre**2;
  }

}

// react 
// pour gerer le cycle de vie des composants react
// useEffect(() =>{}, []);
