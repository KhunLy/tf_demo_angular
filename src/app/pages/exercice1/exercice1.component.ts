import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './exercice1.component.html',
  styleUrls: ['./exercice1.component.scss']
})
export class Exercice1Component implements OnInit {

  secondes: number = 0;
  timer: any = null;

  constructor() { }

  ngOnInit(): void {
  }

  // lorsque l'on cliquera
  start() {
    // on crée un interval qui augmentes le nombre de secondes 1 toutes les secondes
     this.timer = setInterval(() => this.secondes++, 1000);
  }

  stop() {
    clearInterval(this.timer);
    this.timer = null;
  }

}
