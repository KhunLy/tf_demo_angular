import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MenuComponent } from './components/menu/menu.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { BindingsComponent } from './pages/bindings/bindings.component';
import { Exercice1Component } from './pages/exercice1/exercice1.component';
import { StructDirectiveComponent } from './pages/struct-directive/struct-directive.component';
import { Binding2WaysComponent } from './pages/binding2-ways/binding2-ways.component';
import { FormsModule } from '@angular/forms';
import { Exercice2Component } from './pages/exercice2/exercice2.component';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    HomeComponent,
    AboutComponent,
    BindingsComponent,
    Exercice1Component,
    StructDirectiveComponent,
    Binding2WaysComponent,
    Exercice2Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, // necessaire pour les formulaires
    ToastModule,
    ButtonModule,
    InputTextModule,
    TableModule,
    CardModule,
    BrowserAnimationsModule,
  ],
  providers: [ MessageService ], // dependency injection
  bootstrap: [AppComponent]
})
export class AppModule { }
